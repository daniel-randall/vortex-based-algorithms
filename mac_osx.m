/*
vortexResult() - computes and returns the
vortex-based mathematics value of the number
(passed as a zero-terminated string) in szRoot
*/
UInt64 vortexResult(char* szRoot)
{
	// initialize overall sum to zero:
	UInt64 r = 0;

	// set string pointer to beginning of string:
	char* szNumber = szRoot;

	// while end-of-string not reached (outer):
	while (*szNumber != 0)
	{
		// while end-of-string not reached (inner):
		while (*szNumber != 0)
		{
			// sample current digit at pointer address:
			char digit = *szNumber;

			// all characters are treated as 0 until
			// recognized as 0-9:
			UInt64 n = 0;

			// if digit is ASCII 0-9,
			if ((digit > 0x2F) && (digit < 0x3A))
			{
				// promote value of n to digit value:
				n = digit - 0x30;
			}

			// add n to overall sum:
			r += n;

			// increment string pointer:
			szNumber++;

			// (uncomment to display the phases):
			// printf("\n %s / %llu / %llu ", szNumber, n, r);
		}

		// if the overall sum is greater than 9,
		if (r > 9)
		{
			// reset string pointer:
			szNumber = szRoot;

			// change the string to match the
			// current value of the overall sum:
			sprintf(szNumber, "%llu", r);

			r = 0;
		}
	}

	// return the final (single-digit) result:
	return r;
}
